# forteza2017.fr Website

## Install

Create a virtualenv:

```sh
pip install pew
pew new forteza2017.fr-venv --python=python3
```

In the virtualenv:

```sh
pip install -r requirements.txt
```

## Development

```sh
pew workon forteza2017.fr-venv
nikola auto
```

Open http://127.0.0.1:8000/