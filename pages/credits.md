<!--
.. title: Crédits
.. slug: credits
.. date: 2017-05-10 14:38:40 UTC+02:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

Ce site web est réalisé à partir de logiciels libres et _open source_ :

* [hledger](http://hledger.org/) Logiciel de comptabilité à base de texte
* [Nikola](https://getnikola.com/), générateur de site statique
* [Bootstrap](http://getbootstrap.com/), squelette HTML, CSS & Javascript
* [GitLab Continuous Integration](https://about.gitlab.com/gitlab-ci/) et [Docker](https://www.docker.com/), pour stocker et générer les pages
* [Nginx](https://www.nginx.com/), serveur web
* [Git](https://git-scm.com/), système décentralisé de gestion de versions
* [Debian GNU/Linux](https://www.debian.org/), système d'exploitation

[Framasoft](https://framasoft.org/) héberge gracieusement :

* le [code source des comptes](https://framagit.org/forteza2017/comptes.forteza2017.fr)
* le [code source du site](https://framagit.org/forteza2017/forteza2017.fr)
