<!--
.. title: Comptes
.. slug: comptes
.. date: 2017-05-13 17:19:40 UTC+02:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
-->

<div data-social-share-privacy='true'></div>
<br>

Les comptes de la campagne sont publics et mis à jour en temps réel.

Les [données brutes sont disponibles (open data)](https://framagit.org/forteza2017/comptes.forteza2017.fr/blob/master/Comptes_Forteza_2017.journal) au [format texte](http://plaintextaccounting.org/).

<br>
<iframe height="930" src="https://comptes.forteza2017.fr/journal" width="100%"></iframe>
<br>
